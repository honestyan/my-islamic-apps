import React from "react";
import { Button, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

const Stack = createStackNavigator();

import Home from "../screen/home.js";
import Login from "../screen/login.js";
import Form from "../screen/form.js";
import Splash from "../screen/splash.js";
import Detail from "../screen/detail.js";
import About from "../screen/about.js";

const Drawer = createDrawerNavigator();
function Router() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Splash"
        headerMode={"none"}
        screenOptions={{
          cardStyle: { backgroundColor: "white" },
          animationEnabled: false,
        }}
      >
        <Stack.Screen name="MyDrawer" component={MyDrawer} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Form" component={Form} />
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen name="Detail" component={Detail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const MyDrawer = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={Home} />
    <Drawer.Screen name="About" component={About} />
  </Drawer.Navigator>
);
export default Router;
