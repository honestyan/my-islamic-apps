import React from "react";
import { StyleSheet } from "react-native";
import Router from "./Router";
import { StatusBar } from "expo-status-bar";

import Home from "./screen/home.js";
import Login from "./screen/login.js";
import Form from "./screen/form.js";
import Splash from "./screen/splash.js";
import Detail from "./screen/detail.js";
import About from "./screen/about.js";

export default function index() {
  return (
    <>
      <StatusBar />
      <Router />
    </>
  );
}

const styles = StyleSheet.create({});
