import React from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text } from "react-native";

const Login = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.name}>My Islamic</Text>
      <Text style={styles.jargon}>
        Learn Quran and {"\n"} Recite Once Everyday
      </Text>
      <Image style={styles.masjid} source={require("../assets/masjid.png")} />
      <View style={styles.containerBottom}>
        <TouchableOpacity style={styles.register}>
          <Text style={styles.registerText}>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.login}
          onPress={() => props.navigation.navigate("Form")}
        >
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.login}
          onPress={() =>
            props.navigation.navigate("MyDrawer", {
              screen: "Home",
              params: { screen: "About", guest: "Guest" },
            })
          }
        >
          <Text style={styles.loginText}>Login as Guest</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: "45%",
  },
  name: {
    fontSize: 36,
    color: "#672CBC",
    fontWeight: "bold",
    alignSelf: "center",
  },
  jargon: {
    width: 219,
    height: 49,
    fontSize: 20,
    marginTop: 23,
    color: "#A1A1A1",
    textAlign: "center",
  },
  masjid: {
    width: 325,
    height: 181,
    marginTop: 135,
  },
  containerBottom: {
    width: "100%",
    height: 265,
    marginTop: 20,
    backgroundColor: "#672CBC",
    borderRadius: 20,
  },
  register: {
    alignItems: "center",
    width: 346,
    height: 48,
    backgroundColor: "white",
    borderRadius: 20,
    justifyContent: "center",
    marginLeft: 22,
    marginTop: 40,
  },
  registerText: {
    textAlign: "center",
    color: "#672CBC",
    fontWeight: "bold",
    fontSize: 18,
  },
  login: {
    alignItems: "center",
    width: 346,
    height: 48,
    backgroundColor: "#672CBC",
    borderRadius: 20,
    justifyContent: "center",
    marginLeft: 22,
    marginTop: 10,
    borderColor: "white",
    borderWidth: 2,
  },
  loginText: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: 18,
  },
});

export default Login;
