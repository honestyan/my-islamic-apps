import React from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text } from "react-native";

const About = (props) => {
  return (
    <>
      <View style={styles.top}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={{ width: 27, height: 27 }}
            source={require("../assets/back.png")}
          />
        </TouchableOpacity>
        <Image
          style={{ width: 210, height: 62 }}
          source={require("../assets/sanber.png")}
        />
      </View>
      <View style={styles.background}>
        <Text style={styles.title}>
          About <Text style={{ color: "#F4B30F" }}>Me</Text>
        </Text>
        <Image style={styles.ava} source={require("../assets/ava.png")} />
        <Text
          style={{
            textAlign: "center",
            fontSize: 24,
            fontWeight: "bold",
            color: "white",
          }}
        >
          hnstyan
        </Text>
        <Text
          style={{
            color: "white",
            fontSize: 14,
            marginTop: 11,
            textAlign: "center",
          }}
        >
          Honestyan Didyafarhan Atthariq
        </Text>
        <Text style={styles.title2}>
          Hi, Welcome to <Text style={{ color: "#F4B30F" }}>my hut</Text>
        </Text>
        <View style={styles.container2}>
          <Text style={styles.konten}>
            Hello my name is Honestyan. Lorem ipsum dolor sit amet, consectetur
            adipiscing elit. Curabitur augue felis, luctus quis fermentum in,
            consequat eget lorem. Duis non blandit arcu. Cras nisl lorem,
            aliquam in suscipit id, venenatis vitae diam. Phasellus a placerat
            erat. Nulla molestie mauris quis felis pharetra, nec lobortis libero
            aliquam.
          </Text>
        </View>
        <Text
          style={{
            fontSize: 24,
            color: "white",
            textAlign: "center",
            fontWeight: "bold",
            marginTop: 20,
          }}
        >
          Get in Touch
        </Text>
        <View style={styles.sosmed}>
          <TouchableOpacity>
            <Image style={styles.icon} source={require("../assets/ig.png")} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image style={styles.icon} source={require("../assets/fb.png")} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image style={styles.icon} source={require("../assets/git.png")} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image style={styles.icon} source={require("../assets/gm.png")} />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  top: {
    marginTop: 50,
    marginLeft: 33,
  },
  background: {
    width: 320,
    height: 660,
    backgroundColor: "#672CBC",
    borderRadius: 20,
    alignSelf: "center",
  },
  title: {
    alignSelf: "center",
    fontSize: 24,
    marginTop: 34,
    color: "white",
    fontWeight: "bold",
  },
  ava: {
    alignSelf: "center",
    marginTop: 30,
  },
  title2: {
    alignSelf: "center",
    fontSize: 24,
    marginTop: 10,
    color: "white",
    fontWeight: "bold",
  },
  container2: {
    width: 265,
    height: 135,
    backgroundColor: "rgba(196, 196, 196, 0.13)",
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 10,
    borderRadius: 10,
  },
  konten: {
    color: "white",
    textAlign: "center",
    fontSize: 14,
  },
  sosmed: {
    flexDirection: "row",
    alignSelf: "center",
  },
  icon: {
    marginTop: 20,
    marginHorizontal: 10,
  },
});

export default About;
