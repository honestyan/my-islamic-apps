import React, { useState, useEffect } from "react";
import {
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  ImageBackground,
} from "react-native";
import axios from "axios";

const Home = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  //   console.log(data);
  const getData = () => {
    axios
      .get("https://al-quran-8d642.firebaseio.com/data.json")
      .then((result) => {
        setData(result.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getData();
  });

  return (
    <>
      <View style={styles.top}>
        <TouchableOpacity>
          <Image
            style={{ width: 27, height: 27 }}
            source={require("../assets/tabmenu.png")}
          />
        </TouchableOpacity>
        <Text style={styles.title}>My Islamic</Text>
        <TouchableOpacity>
          <Image
            style={{ width: 65, height: 65, marginTop: "-30%" }}
            source={require("../assets/search.png")}
          />
        </TouchableOpacity>
      </View>
      <Text style={styles.salam}>Assalamualaikum</Text>
      <Text style={styles.nama}>{props.route.params.guest}</Text>
      <View style={styles.banner}>
        <Image
          style={{ width: 340, height: 140 }}
          source={require("../assets/banner.png")}
        />
        <View style={styles.read}>
          <Image
            style={{
              width: 35,
              height: 21,
              marginTop: "10%",
              marginLeft: "3%",
            }}
            source={require("../assets/book.png")}
          />
          <Text style={styles.last}>Last Read</Text>
        </View>
        <View style={styles.lastSurah}>
          <Text style={styles.textLastSurah}>Al-Baqarah</Text>
          <Text style={styles.textLastAyah}>Ayat No. 21</Text>
        </View>
      </View>
      <View style={styles.navigator}>
        <TouchableOpacity>
          <Text style={styles.content}>Surah</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.contentOff}>Asmaul Husna</Text>
        </TouchableOpacity>

        <TouchableOpacity>
          <Text style={styles.contentOff}>Doa Harian</Text>
        </TouchableOpacity>
      </View>

      <View
        style={{
          borderBottomColor: "#A1A1A1",
          borderBottomWidth: 1,
          width: 340,
          marginLeft: 25,
          marginTop: 5,
        }}
      />
      <View
        style={{
          borderBottomColor: "#672CBC",
          borderBottomWidth: 2,
          width: 68,
          marginLeft: 25,
        }}
      />
      <FlatList
        data={data}
        keyExtractor={(_, index) => index}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() =>
                props.navigation.navigate("Detail", {
                  nomor: item.nomor,
                  nama: item.nama,
                  type: item.type,
                  arti: item.arti,
                  audio: item.audio,
                })
              }
            >
              <View style={styles.mainContent}>
                <View style={styles.persurat}>
                  <View style={styles.borderCont}>
                    <ImageBackground
                      style={styles.border}
                      source={require("../assets/ayat-border.png")}
                    >
                      <Text>{item.nomor}</Text>
                    </ImageBackground>
                  </View>
                  <View style={styles.suratTitle}>
                    <Text style={styles.namaSurat}>{item.nama}</Text>
                    <Text style={styles.infoSurat}>
                      {item.type.charAt(0).toUpperCase() + item.type.slice(1)} -{" "}
                      {item.arti} ({item.ayat})
                    </Text>
                  </View>
                  <View style={styles.ayatCont}>
                    <Text style={styles.ayat}>{item.asma}</Text>
                  </View>
                </View>
                <View
                  style={{
                    borderBottomColor: "#A1A1A1",
                    borderBottomWidth: 1,
                    width: 340,
                    marginLeft: 25,
                    marginTop: 5,
                  }}
                />
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  top: {
    flexDirection: "row",
    marginTop: 50,
    marginLeft: 20,
    justifyContent: "center",
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    color: "#672CBC",
    marginTop: -4,
    marginLeft: 78,
    marginRight: 60,
  },
  banner: {
    alignItems: "center",
    marginTop: 20,
  },
  salam: {
    color: "#A1A1A1",
    fontSize: 18,
    fontWeight: "500",
    marginLeft: 35,
  },
  nama: {
    fontSize: 22,
    fontWeight: "bold",
    marginLeft: 35,
    textTransform: "capitalize",
  },
  read: {
    flexDirection: "row",
    marginTop: -180,
    marginLeft: -210,
  },
  last: {
    marginTop: 60,
    marginLeft: 5,
    color: "white",
    fontSize: 14,
  },
  lastSurah: {
    marginLeft: -200,
    marginTop: 20,
  },
  textLastSurah: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
  },
  textLastAyah: {
    color: "#B5B5B5",
    marginTop: 5,
  },
  navigator: {
    flexDirection: "row",
    alignSelf: "center",
  },
  content: {
    marginLeft: 23,
    marginTop: 45,
    color: "#672CBC",
    marginHorizontal: "5%",
    fontWeight: "bold",
    fontSize: 18,
  },
  contentOff: {
    marginLeft: 23,
    marginTop: 45,
    color: "#A1A1A1",
    marginHorizontal: "5%",
    fontWeight: "bold",
    fontSize: 18,
  },
  mainContent: {},
  persurat: {
    flexDirection: "row",
    marginBottom: 10,
    marginTop: -5,
  },
  border: {
    width: 52,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
  },
  borderCont: {
    justifyContent: "center",
    marginLeft: 25,
    marginTop: 15,
  },
  namaSurat: {
    fontWeight: "bold",
    fontSize: 16,
    marginLeft: 8,
    marginTop: 21,
  },
  infoSurat: {
    color: "#9A9A9A",
    fontSize: 14,
    marginLeft: 8,
    width: 180,
  },
  ayat: {
    textAlign: "right",
    fontSize: 28,
    color: "#672CBC",
    marginRight: 32,
    marginTop: 25,
  },
  ayatCont: {
    flex: 1,
  },
});

export default Home;
