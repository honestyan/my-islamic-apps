import React from "react";
import { TouchableOpacity, Image, StyleSheet, View, Text } from "react-native";

const Splash = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.name}>My Islamic</Text>
      <Text style={styles.jargon}>
        Learn Quran and {"\n"} Recite Once Everyday
      </Text>
      <Image style={styles.splash} source={require("../assets/splash.png")} />
      <TouchableOpacity
        style={styles.getstarted}
        onPress={() => props.navigation.navigate("Login")}
      >
        <Text style={styles.getstarttext}>Get Started</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  name: {
    fontSize: 36,
    color: "#672CBC",
    fontWeight: "bold",
    alignSelf: "center",
  },
  jargon: {
    width: 219,
    height: 49,
    fontSize: 20,
    marginTop: 23,
    color: "#A1A1A1",
    textAlign: "center",
  },
  splash: {
    width: 312,
    height: 444,
    marginTop: 46,
  },
  getstarted: {
    backgroundColor: "#F9B090",
    width: 196,
    height: 51,
    justifyContent: "center",
    borderRadius: 20,
    marginTop: -25,
  },
  getstarttext: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: 16,
  },
});

export default Splash;
