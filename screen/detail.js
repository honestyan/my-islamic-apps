import React, { useState, useEffect } from "react";
import {
  FlatList,
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
  Text,
} from "react-native";
import axios from "axios";
import HTML from "react-native-render-html";

const Detail = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [latin, setLatin] = useState("");
  //   console.log(data);
  const getData = () => {
    axios
      .get(
        `https://al-quran-8d642.firebaseio.com/surat/${props.route.params.nomor}.json?print=pretty`
      )
      .then((result) => {
        setData(result.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    getData();
  });
  return (
    <>
      <View style={styles.top}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <Image
            style={{ width: 27, height: 27 }}
            source={require("../assets/back.png")}
          />
        </TouchableOpacity>
        <Text style={styles.title}>My Islamic</Text>
        <TouchableOpacity>
          <Image
            style={{ width: 65, height: 65, marginTop: "-30%" }}
            source={require("../assets/search.png")}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.bannerContainer}>
        <Image
          style={styles.banner}
          source={require("../assets/banner2.png")}
        />
        <View style={styles.bannerText}>
          <Text></Text>
          <Text style={styles.bannerTitle}>{props.route.params.nama}</Text>
          <Text style={styles.bannerInfo}>{props.route.params.nomor} Ayat</Text>
          <Text style={styles.bannerInfo}>
            {props.route.params.type.charAt(0).toUpperCase() +
              props.route.params.type.slice(1)}{" "}
            - {props.route.params.arti}
          </Text>
        </View>
      </View>
      <FlatList
        data={data}
        keyExtractor={(_, index) => index}
        renderItem={({ item }) => {
          return (
            <View style={styles.mainContent}>
              <View style={styles.bar}>
                <Text style={styles.nomorAyat}>{item.nomor}</Text>
                <TouchableOpacity>
                  <Image
                    style={styles.play}
                    source={require("../assets/play.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.ayatCont}>
                <Text style={styles.ayat}>{item.ar}</Text>
                <View style={styles.latin}>
                  <HTML html={item.tr} baseFontStyle={{ color: "grey" }} />
                </View>

                <Text style={styles.indo}>{item.id}</Text>
              </View>
            </View>
          );
        }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  top: {
    flexDirection: "row",
    marginTop: 50,
    marginLeft: 20,
    justifyContent: "center",
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    color: "#672CBC",
    marginTop: -4,
    marginLeft: 78,
    marginRight: 60,
  },
  banner: {
    width: 322,
    height: 163,
    alignSelf: "center",
  },
  bannerText: {
    position: "absolute",
    alignItems: "center",
    alignSelf: "center",
    width: 200,
    height: 200,
  },
  bannerTitle: {
    fontSize: 20,
    color: "white",
    fontWeight: "bold",
  },
  bannerInfo: {
    fontSize: 14,
    color: "white",
  },
  bar: {
    flexDirection: "row",
    backgroundColor: "rgba(103, 44, 188, 0.1)",
    width: 325,
    height: 42,
    borderRadius: 20,
    alignSelf: "center",
    marginTop: 20,
  },
  play: {
    width: 30,
    height: 32,
    marginLeft: 125,
    marginTop: 4,
  },
  nomorAyat: {
    height: 30,
    width: 30,
    fontSize: 20,
    color: "white",
    borderRadius: 1000,
    backgroundColor: "#672CBC",
    marginLeft: 20,
    marginRight: 100,
    alignSelf: "center",
    textAlign: "center",
  },
  ayat: {
    textAlign: "right",
    fontSize: 30,
    color: "#672CBC",
    marginTop: 7,
    fontWeight: "bold",
  },
  latin: {
    color: "#A2A2A2",
    fontSize: 16,
    marginTop: 10,
  },
  indo: {
    fontSize: 16,
    marginTop: 5,
  },
  mainContent: {
    paddingHorizontal: 40,
  },
});

export default Detail;
