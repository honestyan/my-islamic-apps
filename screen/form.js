import React, { useState } from "react";
import {
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
  Text,
} from "react-native";

const Form = ({ props, navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <Text style={styles.name}>My Islamic</Text>
      <Text style={styles.jargon}>
        Learn Quran and {"\n"} Recite Once Everyday
      </Text>
      <Image style={styles.masjid} source={require("../assets/masjid.png")} />
      <View style={styles.containerBottom}>
        <TextInput
          style={styles.register}
          //   onChangeText={onChangeNumber}
          //   value={number}
          placeholder="Username"
          placeholderTextColor={"white"}
          TextInput
          value={username}
          onChangeText={(val) => setUsername(val)}
        />
        <TextInput
          style={styles.register}
          //   onChangeText={onChangeNumber}
          //   value={number}
          placeholder="Password"
          placeholderTextColor={"white"}
          TextInput
          value={password}
          onChangeText={(val) => setPassword(val)}
        />
        <TouchableOpacity
          style={styles.login}
          onPress={() =>
            navigation.navigate("MyDrawer", {
              screen: "Home",
              params: { guest: username },
            })
          }
        >
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: "45%",
  },
  name: {
    fontSize: 36,
    color: "#672CBC",
    fontWeight: "bold",
    alignSelf: "center",
  },
  jargon: {
    width: 219,
    height: 49,
    fontSize: 20,
    marginTop: 23,
    color: "#A1A1A1",
    textAlign: "center",
  },
  masjid: {
    width: 325,
    height: 181,
    marginTop: 135,
  },
  containerBottom: {
    width: "100%",
    height: 265,
    marginTop: 20,
    backgroundColor: "#672CBC",
    borderRadius: 20,
    paddingTop: 30,
  },

  registerText: {
    textAlign: "center",
    color: "white",
    fontWeight: "bold",
    fontSize: 18,
  },
  login: {
    alignItems: "center",
    width: 346,
    height: 48,
    backgroundColor: "white",
    borderRadius: 20,
    justifyContent: "center",
    marginLeft: 22,
    marginTop: 10,
  },
  loginText: {
    textAlign: "center",
    color: "#672CBC",
    fontWeight: "bold",
    fontSize: 18,
  },
  register: {
    alignItems: "center",
    width: 346,
    height: 48,
    borderRadius: 20,
    justifyContent: "center",
    marginLeft: 22,
    marginTop: 10,
    borderColor: "white",
    color: "white",
    borderWidth: 2,
    paddingLeft: 20,
  },
});

export default Form;
